const {user_game, user_game_biodata, user_game_history} = require ('../models')
// const passport = require('../lib/passport')
// const passportJWT = require('../lib/passport-jwt')

module.exports = {
    //password di add user belum ter-encrypt, pakai user_game.signup & password: bcrypt.hashSync(password, 10) => belum bisa
    //password di add user sudah terencrypt dg tambahan syntax @model: user_games
    addUser: (req,res) => {
        const {first_name, last_name, birth_place, username, password} = req.body;
        user_game.addUser({
            username,
            password,
            isSuperAdmin: false
        }).then (user_game => {
            user_game_biodata.create({
                user_id: user_game.id,
                first_name,
                last_name,
                birth_place
            })
            .then(response =>{
                res.redirect('/dashboard')
            })
        })
    },
    viewDelUser: (req, res) => {
        const {id} = req.params
      
        user_game.destroy({
            where: {id}
        }).then (response => {
            user_game_biodata.destroy({
                where: {user_id: id}
            }).then (response => {
                res.redirect('/dashboard')
            })
        })
    },
    viewEditUser: (req, res) => {
        const {id} = req.params
      
        user_game.findOne({
            where: {id},
            include: user_game_biodata
        }).then(user => {
            res.render('edit', {user})
        })
    },
    editUser: (req, res) => {
    const {id} = req.params
    const {first_name, last_name, birth_place, username, password} = req.body

    user_game.update({
        username,
        password
    }, {where : {id}})
    .then(response => {
        user_game_biodata.update({
            first_name,
            last_name,
            birth_place
        }, {where: {user_id: id}})
        .then(response =>{
            res.redirect('/dashboard')
        })
    })
    }
}