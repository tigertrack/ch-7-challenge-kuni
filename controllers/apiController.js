const {user_game} =  require('../models')
const format = (user) => {
    const { id, username } = user
    
    return {
        id,
        username,
        accessToken: user.generateToken()
    }
}


module.exports = {
    apiLogin: (req, res) => {
        user_game.authenticate(req.body)
        .then(user => {
            res.json(format(user))
        })
    },
    apiHome: (req, res) => {
        const {username} = req.user.dataValues
        res.send(`Welcome to protected api section, ${username}`)
    },
    apiMe: (req, res) => {
        const currentUser = req.user.dataValues
        res.json(currentUser)
    },
    createRoom: (req, res) => {
        const {username, id} = req.user.dataValues
        res.send (`${username} sudah memmbuat room id ${id}`) //masih menggunakan id user_games
        // const {username} = req.user.dataValues
        // res.send(`${username} sudah create room`)

        //get player id for room creator data
        //create & insert room into the db
        //return room id

        //maaf banget mas..belum bisa implement pvp nya, masih butuh waktu untuk mencerna penjelasannya..
    },
    joinRoom: (req, res) => {
        
    }
}