const {user_game, user_game_biodata, user_game_history} = require ('../models')
const passport = require('../lib/passport')
const passportJWT = require('../lib/passport-jwt')

module.exports = {
    viewHome: (req, res) => {
            res.render('home')
    },
    viewLogin: (req, res) => {
        res.render('login')
    },
    viewSignup: (req, res) => {
        res.render('signup')
    },
    viewContact: (req, res) => {
        res.render('contact')
    },
    viewWork: (req, res) => {
        res.render('work')
    },
    viewAboutus: (req, res) => {
        res.render('aboutUs')
    },
    viewGamesuit: (req, res) => {
        res.render('gamesuit')
    },
    viewDashboard: (req, res) => {
        user_game.findAll({
          include: user_game_biodata
        })
        .then(users => {
          res.render("dashboard", {
            users})
        })
    },
    //belum authorization isSuperadmin vs common player
    login: passport.authenticate('local', {
        successRedirect:'/gamesuit',
        failureRedirect: '/login',
        failureFlash: true //untuk mengaktifkan express flash
    }),
   
    // login: async (req, res) => {
    //     const {username, password} = req.body;
    
    //     user_game.findOne({
    //         where:{
    //             username: username,
    //             password: password
    //         }
    //     }).then(response => {
    //         if (response != null && response.isSuperAdmin == true){
    //             res.redirect('/dashboard')
    //         }else if(response !=null){
    //             res.redirect('/gamesuit')
    //         }
    //         else{
    //             //alert("User belum terdata");
    //             res.redirect('/login')
    //         }
    //     })   
    // },
    signup: (req,res) => {
        const {first_name, last_name, birth_place, username, password} = req.body;
        user_game.signup({
            username,
            password,
            isSuperAdmin: false
        }).then (user_game => {
            user_game_biodata.create({
                user_id: user_game.id,
                first_name,
                last_name,
                birth_place
            })
            .then(response =>{
                res.redirect('/login')
            })
        })
    }
}